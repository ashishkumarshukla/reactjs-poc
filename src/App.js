import React, { Component } from 'react';
import './App.css';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';
import Validation from './Validation/Validation';
import Char from './Char/Char';


class App extends Component {

  constructor(props){
    super(props);
    console.log('[App.js] constructor called');
    }
 
   state={
     username:'Ashish!',
     enterText: ''
    }

   static getDerivedStateFromProps(props,state){
    console.log('[App.js] getDerivedStateFromProps called',props);
    return state;
    }

    componentDidMount(){
      console.log('[App.js] ComponentDidMount');
    }

    shouldComponentUpdate(nextProps,nextState){
      console.log('[App.js] shouldComponentUpdate called');
      return true;
  }

  getSnapshotBeforeUpdate(prevProps,prevState){
      console.log('[App.js] getSnapshotBeforeUpdate called');
      return {message:'Hi from App'}
  }

  componentDidUpdate(prevProps,PrevState,snapshot){
      console.log('[App.js] componentDidUpdate called');
      console.log(snapshot);
  }


   userNameChangeEventHandler=(event)=>{
     this.setState({
     username: event.target.value
                 })
             }

    inputChangeHandler=(event)=>{
        this.setState({
        enterText: event.target.value
        })
      }

    deleteCharHandler=(index)=>{
      const text=this.state.enterText.split('');
      text.splice(index,1);
      const updatedText=text.join('');
      this.setState({
      enterText:updatedText
        });
      }


  render() {
    console.log('[App.js] rendering');

    const charList=this.state.enterText.split('').map((ch,index)=>{
      return <Char character={ch} 
              key={index}
              clicked={()=>this.deleteCharHandler(index)}/>;

    });
    return (
      <div className="App">
        <h1>Please Enter Your Name.</h1>
        <UserInput onchange={this.userNameChangeEventHandler} username={this.state.username}/>
        <UserOutput name={this.state.username}/>
        <hr/>
        <input type="text" onChange={this.inputChangeHandler} value={this.state.enterText}></input>
        <p>The length of the entered text is {this.state.enterText.length}</p>
        <Validation inputLength={this.state.enterText.length}/>
        <hr/>
        {charList}
      </div>
    );
  }
}

export default App;
