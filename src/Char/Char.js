import React from 'react';

const char=(props)=>{
     console.log('[Char.js] rendering');
    const style={
        display:'inline-block',
        padding:'2px',
        textAlign:'center',
        margin:'20px',
        border:'2px solid red',
        width:'30px'
};
    return(
<div style={style} onClick={props.clicked}>{props.character}</div>
    );
}

export default char;