import React,{useEffect} from 'react';

const userinput=(props) =>{
   
    console.log('[UserInput.js] rendering');
    useEffect(()=>{
        console.log('[UserInput.js] useEffect');
        setTimeout(()=>{
            alert('user input data saved');
        },1000);
        return ()=>{
            console.log('[UserInput.js] cleanUp');
        };
    },[props.username]);


    const style={
        border:'2px solid red'
    };

    return (
     <input type="text"  style={style} onChange={props.onchange} value={props.username}></input>
    );
}

export default userinput;