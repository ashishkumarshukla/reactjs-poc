import React,{Component} from 'react';
import './UserOutput.css';

class  UserOutput extends Component{

    constructor(props){
        super(props);
        console.log('[UserOutput.js] constructor called');
        this.state={name:'Akka'}
        }

        static getDerivedStateFromProps(props,state){
            console.log('[UserOutput.js] getDerivedStateFromProps called');
            return state;
        }

        shouldComponentUpdate(nextProps,nextState){
            console.log('[UserOutput.js] shouldComponentUpdate called');
            return nextProps.name!==this.props.name;
            
        }

        getSnapshotBeforeUpdate(prevProps,prevState){
            console.log('[UserOutput.js] getSnapshotBeforeUpdate called');
            return {message:'Hi'}
        }

        componentDidUpdate(prevProps,PrevState,snapshot){
            console.log('[UserOutput.js] componentDidUpdate called');
            console.log(snapshot);
        }
        componentWillUnmount(){
            console.log('[UserOutput.js] componentWillUnmount');
          }

    render(){
    console.log('[UserOutput.js] rendering');
    return (
        <p className="UserOutput">Hi {this.props.name}</p>
         );
        }
}

export default UserOutput;